const input = document.querySelector('[data-testid="fruit-input"]')

input.onkeyup = (event) => {
    if (event.key === "Enter") {
        const li = document.createElement('li')
        li.innerText = input.value
        document.querySelector('.fruit-list').append(li)
    }
}