/// <reference types="cypress"/>

beforeEach(() => {

  cy.visit("http://localhost:5500")
  cy.viewport(400, 400)

})

describe('Queries', () => {


  it('Should find our title', () => {

    // Hitta element via dess taggnamn
    cy.get("h2")

    // Hitta element via dess klass / class
    cy.get(".header")

    // Hitta element via dess id
    cy.get("#header")

    // Hitta element via attribut data-testid 
    cy.get("[type='password']")

    // Hitta element via attribut och elementtag
    cy.get("input[type='password']")

    // Hitta element via attributet data-testid
    cy.get("[data-testid='password-field']")

    // Genom mer avancerade CSS-Selektorer
    cy.get('.fruit-list > li:last')

    cy.get('.fruit-list > :nth-child(2)')
  })

  it('Should find element containing something', () => {

    cy.contains("Min hemsid")

    cy.contains(/an/)

    cy.get("li:contains(an)")

    cy.get(".fruit-list").contains(/i/)

  })

  it('Should traverse our elements', () => {

    // Find - Hittar element i det queriade elementet
    cy.get(".fruit-list").find("li")

    // First - hittar vi det första elementet ur en lista
    cy.get(".fruit-list").find("li").first()

    // Last - hittar det sista elementer ur en lista av element
    cy.get(".fruit-list").find("li").last()

    // Next - hittar nästa element på tur bland sibling
    cy.get(".fruit-list").find("li").first().next()

    // NextAll - hittar alla element efter det vi stod på
    cy.get(".fruit-list").find("li").first().nextAll()

    // NextUntil - hittar alla element efter det vi stod på tills ett villkor
    cy.get(".fruit-list").find("li").first().nextUntil(":contains(o)")

    // eq - hittar ett element utifrån dess index i listan av element
    cy.get(".fruit-list").find('li').eq(2)

    // Filter - hittar element som matchar ett visst villkor
    cy.get(".fruit-list").find('li').filter(":contains(an)")

    // Not - hittar element som inte matchar ett visst villkor
    cy.get(".fruit-list").find('li').not(":contains(an)")

  })


})

describe('Actions', () => {

  it('Should interact with our elements', () => {

    // Type - skriv in text
    cy.get('[data-testid="fruit-input"]').type("Enbär")

    // Clear - rensa text
    cy.get('[data-testid="fruit-input"]').clear()

    // Vissa fält kräver att vi använder specifik syntax
    cy.get('[type="date"]').type("2026-12-24")

    // Använda vissa specialtangter
    cy.get('[data-testid="fruit-input"]').type("Blomkål{enter}", {
      delay: 10
    })

  })

  it('Should click something', () => {

    cy.get('input[type="checkbox"]').check()

    cy.get('input[type="checkbox"]').uncheck()

    cy.get('button').click()

  })



})

describe('Assertions', () => {

  it("Should add fruits to the bottom of the list", () => {
    // Arrange
    const fruit = "Enbär"

    // Act
    cy
      .get('[data-testid="fruit-input"]')
      .type(fruit)
      .type('{enter}')

    // Assertion
    cy.get(".fruit-list")
      .find('li')
      .should('have.length', 6)
      .last()
      .should('contain.text', fruit)

    cy.get(".fruit-list")
      .find('li')
      .last()
      .prev()
      .invoke('text')
      .then(text => {
        const number = +text.replace(/\D/g, "")
        cy.log(number)
        return cy.wrap(number)
      })
      .should('be.greaterThan', 5)

  })

})